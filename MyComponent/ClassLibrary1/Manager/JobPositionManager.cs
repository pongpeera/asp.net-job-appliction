﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using JobApplication.Util;
using JobApplication.Bean;

namespace JobApplication.Manager
{
    public class JobPositionManager
    {
        public JobPosition jobPosition { get; set; }
        public bool AddJobPosition()
        {
            int row = 0;
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.CommandText = "[dbo].[p_insertJobPosition]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@name", jobPosition.jobPositionName));
                cmd.Parameters.Add(new SqlParameter("@description", jobPosition.jobPositionDescription));
                row = cmd.ExecuteNonQuery();
                conn.Close();
            }
            return (row > 0);
        }

        public List<JobPosition> GetAllJobPosition()
        {
            List<JobPosition> jobs = new List<JobPosition>();
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.CommandText = "[dbo].[p_getAllJobPosition]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    jobs.Add(new JobPosition(
                            reader.GetInt32(0),
                            reader.GetString(1),
                            reader.GetString(2)
                        ));
                }
                conn.Close();
            }
            return jobs;
        }

        public bool RemoveJobPosition()
        {
            int row = 0;
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.CommandText = "[dbo].[p_deleteJobPosition]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@id", jobPosition.jobPositionId));
                row = cmd.ExecuteNonQuery();
                
                conn.Close();
            }
            return (row > 0);
        }
    }
}
