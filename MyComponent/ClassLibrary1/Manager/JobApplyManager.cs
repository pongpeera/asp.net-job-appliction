﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobApplication.Bean;
using JobApplication.Util;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;

namespace JobApplication.Manager
{
    public class JobApplyManager
    {
        public JobApply jobApply { get; set; }

        public JobApplyManager()
        {
            this.jobApply = new JobApply();
        }
        public bool ApplyJobApplication()
        {
            int row = 0;
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.CommandText = "[dbo].[p_insertProfileAndApply]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@login_email", jobApply.profile.login.email));
                cmd.Parameters.Add(new SqlParameter("@login_password", jobApply.profile.login.password));
                cmd.Parameters.Add(new SqlParameter("@login_status", 1 ));
                cmd.Parameters.Add(new SqlParameter("@user_thai_fname", jobApply.profile.thaiName));
                cmd.Parameters.Add(new SqlParameter("@user_thai_lname", jobApply.profile.thaiLastName));
                cmd.Parameters.Add(new SqlParameter("@user_eng_fname", jobApply.profile.engName));
                cmd.Parameters.Add(new SqlParameter("@user_eng_lname", jobApply.profile.engLastName));
                cmd.Parameters.Add(new SqlParameter("@user_gender", jobApply.profile.gender));
                cmd.Parameters.Add(new SqlParameter("@user_birthdate", jobApply.profile.birthdate));
                cmd.Parameters.Add(new SqlParameter("@user_address", jobApply.profile.address));
                cmd.Parameters.Add(new SqlParameter("@user_telephonenumber", jobApply.profile.telephoneNumber));
                cmd.Parameters.Add(new SqlParameter("@user_education", jobApply.profile.education));
                cmd.Parameters.Add(new SqlParameter("@user_personalid", jobApply.profile.personalId));
                cmd.Parameters.Add(new SqlParameter("@user_img_name", jobApply.profile.image));
                cmd.Parameters.Add(new SqlParameter("@apply_date", jobApply.applyDate));
                cmd.Parameters.Add(new SqlParameter("@apply_position", jobApply.position.jobPositionId));
                cmd.Parameters.Add(new SqlParameter("@apply_salay_require", jobApply.salaryRequire));
                cmd.Parameters.Add(new SqlParameter("@apply_start_date", jobApply.startDate));
                cmd.Parameters.Add(new SqlParameter("@apply_resume", jobApply.hasResume));
                row = cmd.ExecuteNonQuery();
                conn.Close();
            }
            return (row > 0);
        }
        public List<JobApply> GetAllApply()
        {
            List<JobApply> applyList = new List<JobApply>();
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM [dbo].[v_GetAllJobApply]";            
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    JobApply apply = new JobApply();
                   
                    apply.profile.thaiName = reader.GetString(0);
                    apply.profile.thaiLastName = reader.GetString(1);
                    apply.profile.login.email = reader.GetString(2);
                    apply.profile.education = reader.GetString(3);
                    apply.profile.telephoneNumber = reader.GetString(4);
                    apply.profile.gender = reader.GetInt32(5);
                    apply.profile.birthdate = reader.GetString(6);
                    apply.position.jobPositionName = reader.GetString(7);
                    apply.applyDate = reader.GetString(8);
                    apply.isConfirm = reader.GetInt32(9);
                    apply.result = reader.GetInt32(11);

                    if (reader.GetInt32(10) > 0)
                    {
                        apply.interviewList.Add(new Interview());
                    }
                    applyList.Add(apply);

                }
                conn.Close();
            }
            return applyList;
        }

        public List<JobApply> GetApply()
        {
            List<JobApply> applyList = new List<JobApply>();
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM [dbo].[v_GetAllJobApply] WHERE user_email = '"+ jobApply.profile.login.email+"'";
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    JobApply apply = new JobApply();
                    apply.profile.thaiName = reader.GetString(0);
                    apply.profile.thaiLastName = reader.GetString(1);
                    apply.profile.login.email = reader.GetString(2);
                    apply.profile.education = reader.GetString(3);
                    apply.profile.telephoneNumber = reader.GetString(4);
                    apply.profile.gender = reader.GetInt32(5);
                    apply.profile.birthdate = reader.GetString(6);
                    apply.position.jobPositionName = reader.GetString(7);
                    apply.applyDate = reader.GetString(8);
                    apply.isConfirm = reader.GetInt32(9);
                    if (reader.GetInt32(10) > 0)
                    {
                        apply.interviewList.Add(new Interview(reader.GetString(12), reader.GetString(13)));
                    }
                    applyList.Add(apply);
                }
                conn.Close();
            }
            return applyList;
        }

        public bool ConfirmApply()
        {
            var row = 0;
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();
                var cmd = new SqlCommand();
                cmd.CommandText = "[dbo].[p_confirmApply]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@email", jobApply.profile.login.email));
                row = cmd.ExecuteNonQuery();
                conn.Close();
            }
            return (row > 0);
        }
        
        public bool ConfirmInterview()
        {
            var row = 0;
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();
                var cmd = new SqlCommand();
                cmd.CommandText = "[dbo].[p_confirmInterview]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@email", jobApply.profile.login.email));
                cmd.Parameters.Add(new SqlParameter("@apply_date", jobApply.applyDate));
                foreach (Interview interview in jobApply.interviewList)
                {
                    cmd.Parameters.Add(new SqlParameter("@date", interview.interviewDate));
                    cmd.Parameters.Add(new SqlParameter("@time", interview.interviewTime));
                }
               
                row = cmd.ExecuteNonQuery();
                conn.Close();
            }
            return (row > 0);
        }

        public void SendMail(string subject,string body)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(jobApply.profile.login.email);
            mailMessage.From = new MailAddress("pongpeera.love@gmail.com");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
            smtpClient.Send(mailMessage);

        }

        public bool ConfirmResult()
        {
            var row = 0;
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();
                var cmd = new SqlCommand();
                cmd.CommandText = "[dbo].[confirmResult]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@email", jobApply.profile.login.email));
                cmd.Parameters.Add(new SqlParameter("@result", jobApply.result));
                row = cmd.ExecuteNonQuery();
                conn.Close();
            }
            return (row > 0);
        }
    }

    
}
