﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobApplication.Bean;
using JobApplication.Util;
using System.Data.SqlClient;
using System.Data;

namespace JobApplication.Manager
{
    
    public class JobLoginManager
    {
        public Login login { get; set; }

        public JobLoginManager()
        {
            this.login = new Login();
        }
        
        public bool AddNewAdmin()
        {
            int row = 0;
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.CommandText = "[dbo].[p_insertNewAdmin]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@email", login.email));
                cmd.Parameters.Add(new SqlParameter("@password", login.password));
                row = cmd.ExecuteNonQuery();

                conn.Close();
            }
            return (row > 0);
        }
        public bool verifyLogin()
        {
            string password = "";
            using (var conn = DatabaseConnection.GetConnection())
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.CommandText = "[dbo].[p_verifyLogin]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@email", login.email));
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    password = reader.GetString(0);
                    login.status = reader.GetInt32(1);
                }
               

                conn.Close();
            }

            return login.password.Equals(password);
        }

    }
}
