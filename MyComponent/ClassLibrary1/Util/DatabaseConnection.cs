﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;



namespace JobApplication.Util
{
    class DatabaseConnection
    {
        public static SqlConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["JobApplicationDB"].ConnectionString;
            var conn = new SqlConnection(conString);
            return conn;
        }


    }
}
