﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobApplication.Bean
{
    public class Login
    {
        public string email { get; set; }
        public string password { get; set; }
        public int status { get; set; }
        public UserProfile profile { get; set; }

        public Login(string email,string password)
        {

            this.email = email;
            this.password = password;
        }

        public Login()
        { }
    }
}
