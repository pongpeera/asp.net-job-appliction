﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobApplication.Bean
{
    public class UserProfile
    {
        public string thaiName { get; set; }
        public string thaiLastName { get; set; }
        public string engName { get; set; }
        public string engLastName { get; set; }
        public int gender { get; set; }
        public string birthdate { get; set; }
        public string telephoneNumber { get; set; }
        public string education { get; set; }
        public string personalId { get; set; }
        public string image { get; set; }
        public string address { get; set; } 
        public Login login { get; set; }
        public List<JobPosition> jobPositions { get; set; }

        public UserProfile()
        {
            this.login = new Login();
            jobPositions = new List<JobPosition>();
        }

    }
}
