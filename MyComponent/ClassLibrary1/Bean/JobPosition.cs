﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobApplication.Bean
{
    public class JobPosition
    {
        public int jobPositionId { get; set; }
        public string jobPositionName { get; set; }
        public string jobPositionDescription { get; set; }

        public JobPosition()
        { }
        public JobPosition(int id )
        {
            this.jobPositionId = id;
        }

        public JobPosition(string name)
        {
            this.jobPositionName = name;
        }
        public JobPosition(string name,string description)
        {
            this.jobPositionName = name;
            this.jobPositionDescription = description;
        }

        public JobPosition(int id, string name, string description)
        {
            this.jobPositionId = id;
            this.jobPositionName = name;
            this.jobPositionDescription = description;
        }
    }
}
