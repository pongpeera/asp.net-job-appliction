﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobApplication.Bean
{
    public class JobApply
    {
        public string applyDate { get; set; }
        public int isConfirm { get; set; }
        public int hasResume { get; set; }
        public float salaryRequire { get; set; }
        public string startDate { get; set; }
        public int result { get; set; }
        public JobPosition position { get; set; }
        public UserProfile profile { get; set; }
        public List<Interview> interviewList { get; set; }
             
        public JobApply()
        {
            this.position = new JobPosition();
            this.profile = new UserProfile();
            this.interviewList = new List<Interview>();
        }
        
        
    }
}
