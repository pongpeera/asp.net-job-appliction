﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobApplication.Bean
{
    public class Interview
    {
        public string interviewDate { get; set; }
        public string interviewTime { get; set; } 
        public JobApply jobApply { get; set; }

        public Interview()
        {
            
        }

        public Interview(string date,string time)
        {
            this.interviewDate = date;
            this.interviewTime = time;
        }
    }
}
