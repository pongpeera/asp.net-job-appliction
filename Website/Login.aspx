﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="bodyContent" runat="server" ContentPlaceHolderID="bodyContent">
    <div class="content-center w3-display-middle w3-grayscale-min">
        <div class="w3-white w3-hover-shadow ">

            <div class="form-group customFont">
                <header class="w3-container w3-light-green">
                    <h2 class="customFont" style="color:white;">LOGIN</h2>
                </header>
                <br />
                <div class="w3-container w3-opacity">
                    <p>
                        <b><h6><asp:Label Text="Email" runat="server" CssClass="w3-label" ID="lbLoginEmail" /></h6></b>
                        <asp:TextBox ID="loginEmail" runat="server" CssClass="w3-input w3-border" />
                    </p>
                    <p>
                        <b><h6><asp:Label Text="Password" runat="server" CssClass="w3-label" ID="Label1" /></h6></b>
                         <asp:TextBox ID="loginPassword" runat="server" CssClass="w3-input w3-border" TextMode="Password" />
                    </p>
                    <p>
                        <asp:Button ID="loginButton" CssClass="w3-btn-block w3-dark-grey" Text="Login" runat="server" OnClick="loginButton_Click" />
                    </p>
                    <br />
                </div>
            </div>
            
        </div>
    </div>
</asp:Content>
