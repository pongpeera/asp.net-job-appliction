﻿create procedure p_verifyLogin (@email nvarchar(60)) as
begin
	select login_password ,login_status from [dbo].login where login_email = @email;
end;
go;

create procedure p_insertJobPosition (@name nvarchar(60),@description text) as
begin
	insert into position (position_name,position_description) values(@name,@description);
end;
go;

create procedure p_getAllJobPosition as 
begin
	select * from position;
end;
go;


create procedure p_deleteJobPosition (@id int) as
begin
	delete from position where position_id = @id;
end;
go;

create procedure p_insertNewAdmin(@email nvarchar(60),@password nvarchar(30)) as
begin
	insert into login values (@email,@password,0); 
end ;
go;

create procedure p_insertProfileAndApply (
	@login_email nvarchar(60),
	@login_password  nvarchar(30),
	@login_status int,
	@user_thai_fname nvarchar(60),
	@user_thai_lname nvarchar(60),
	@user_eng_fname nvarchar(60),
	@user_eng_lname nvarchar(60),
	@user_gender int,
	@user_birthdate nvarchar(60),
	@user_address text,
	@user_telephonenumber nvarchar(10),
	@user_education nvarchar(60),
	@user_personalid nvarchar(13),
	@user_img_name nvarchar(60),
	@apply_date nvarchar(60),
	@apply_position int,
	@apply_salay_require float,
	@apply_start_date nvarchar(80),
	@apply_resume int
) as
begin
	insert into dbo.login values(@login_email,@login_password,@login_status);
	insert into dbo.user_profile values(
		@user_thai_fname,
		@user_thai_lname,
		@user_eng_fname,
		@user_eng_lname,
		@user_gender,
		@user_birthdate,
		@user_address,
		@user_telephonenumber,
		@login_email,
		@user_education,
		@user_personalid,
		@user_img_name
		
	);
	insert into dbo.apply_job values(
		@apply_date,
		@apply_position,
		@login_email,
		0,
		0,
		0,
		@apply_salay_require,
		@apply_start_date,
		@apply_resume
	);
end;
go;
create procedure dbo.GetAllApply(@email nvarchar(60))
as
begin
	select user_thai_fname,user_thai_lname, user_email  ,user_education,user_telephonenumber ,user_gender ,user_birthdate ,p.position_name ,a.apply_date
	from dbo.user_profile u join dbo.apply_job a on u.user_email = a.apply_email join dbo.position p on p.position_id =  a.apply_postion;
end;
go;

create procedure p_confirmApply (@email nvarchar(60)) as
begin
	UPDATE [dbo].[apply_job] SET apply_confirm = 1 WHERE apply_email = @email;
end;
go;

create procedure p_confirmInterview (@email nvarchar(60),@apply_date nvarchar(60),@date nvarchar(60 ),@time nvarchar(60))
as
begin
	insert into interview values(@apply_date,@date,@time,@email);
	UPDATE [dbo].[apply_job] SET apply_interview = 1 WHERE apply_email = @email;
end;
go;

create procedure confirmResult (@email nvarchar(60),@result int) as
begin
	UPDATE [dbo].[apply_job] SET apply_result = @result WHERE apply_email = @email;
end;
