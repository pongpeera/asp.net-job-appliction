﻿create view v_GetAllJobApply as
select user_thai_fname,user_thai_lname, user_email  ,user_education,user_telephonenumber ,user_gender ,user_birthdate ,p.position_name ,a.apply_date
	,a.apply_confirm ,a.apply_interview , a.apply_result,i.interview_date,i.interview_time
	from dbo.user_profile u join dbo.apply_job a on u.user_email = a.apply_email join dbo.position p on p.position_id =  a.apply_postion
	 left join interview i on i.interview_email = a.apply_email and i.interview_apply = a.apply_date;