﻿create table login 
(
	login_email nvarchar(60) primary key,
	login_password  nvarchar(30) not null,
	login_status int not null
);

create table user_profile
(
	user_thai_fname nvarchar(60) not null,
	user_thai_lname nvarchar(60) not null,
	user_eng_fname nvarchar(60) not null,
	user_eng_lname nvarchar(60) not null,
	user_gender int not null,
	user_birthdate nvarchar(60) not null,
	user_address text not null,
	user_telephonenumber nvarchar(10) not null,
	user_email nvarchar(60) primary key,
	user_education nvarchar(60) not null,
	user_personalid nvarchar(13) not null,
	user_img_name nvarchar(60) null,
	CONSTRAINT FK_login_user_email FOREIGN KEY (user_email)     
    REFERENCES login (login_email)     
    ON DELETE CASCADE
);

create table position 
(
	position_id int IDENTITY(1,1) primary key,
	position_name nvarchar(60),
	position_description text
);

create table apply_job
(
	apply_date nvarchar(60)  ,
	apply_postion int not null,
	apply_email nvarchar(60) ,
	apply_confirm int null,
	apply_interview int null,
	apply_result int null,
	apply_salay_require float not null,
	apply_start_date nvarchar(80),
	apply_resume int null,
	primary key (apply_date,apply_email),
	CONSTRAINT FK_user_apply FOREIGN KEY (apply_email)     
    REFERENCES user_profile (user_email)     
    ON DELETE CASCADE,
	CONSTRAINT FK_user_position FOREIGN KEY (apply_postion)     
    REFERENCES position (position_id)     
    ON DELETE CASCADE
);


create table interview
(
	interview_apply nvarchar(60), 
	interview_date nvarchar(60),
	interview_time nvarchar(60),
	interview_email nvarchar(60),
	primary key (interview_date,interview_time),
	CONSTRAINT FK_apply_interview FOREIGN KEY (interview_apply,interview_email)     
    REFERENCES apply_job (apply_date,apply_email)     
    ON DELETE CASCADE,
);
