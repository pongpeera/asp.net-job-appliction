﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JobApplication.Manager;
using JobApplication.Bean;

public partial class Apply : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false) {
            InitForm();
        }
       
    }

    private void InitForm()
    {
        JobPositionManager manager = new JobPositionManager();
        List<JobPosition> jobs = manager.GetAllJobPosition();
        ddApplayJobPosition.DataTextField = "jobPositionName";
        ddApplayJobPosition.DataValueField = "jobPositionId";
        ddApplayJobPosition.DataSource = jobs;
        ddApplayJobPosition.DataBind();


    }

    protected void applyJobButton_Click(object sender, EventArgs e)
    {
        string appPath = Request.PhysicalApplicationPath;
        JobApplyManager manager = new JobApplyManager();
        manager.jobApply.applyDate = DateTime.Today.ToString("dd/MM/yyyy");
        manager.jobApply.salaryRequire = float.Parse(salaryRequirement.Text);
        manager.jobApply.startDate = startDate.Text;
        manager.jobApply.position.jobPositionId = int.Parse(ddApplayJobPosition.SelectedValue);
        manager.jobApply.hasResume = 0;
       
        manager.jobApply.profile.gender = ddApplyGender.SelectedIndex;
        manager.jobApply.profile.education = ddApplyEducation.SelectedValue;
        manager.jobApply.profile.thaiName = thaiFirstName.Text;
        manager.jobApply.profile.thaiLastName = thaiLastName.Text;
        manager.jobApply.profile.engName = englishFirstName.Text;
        manager.jobApply.profile.engLastName = englishLastName.Text;
        manager.jobApply.profile.personalId = personalId.Text;
        manager.jobApply.profile.telephoneNumber = telephoneNumber.Text;
        manager.jobApply.profile.login.email = email.Text;
        manager.jobApply.profile.login.password = password.Text;
        manager.jobApply.profile.birthdate = Birthdate.Text;
        manager.jobApply.profile.address = address.Text;

        if (profileImage.HasFile)
        {
            string saveDir = "/Images/Profile/";
            string fileExtension = profileImage.FileName.Split('.')[1];
            string fileName = englishFirstName.Text + "_" + englishLastName.Text + "." + fileExtension;
            string savePath = appPath + saveDir + Server.HtmlEncode(fileName);
            
            profileImage.SaveAs(savePath);
            manager.jobApply.profile.image = fileName;

        }

        if (resume.HasFile)
        {
            string saveDir = "/Other_file/Resume/";
            string fileExtension = resume.FileName.Split('.')[1];
            string fileName = englishFirstName.Text + "_" + englishLastName.Text + "." + fileExtension;
            string savePath = appPath + saveDir + Server.HtmlEncode(fileName);

            profileImage.SaveAs(savePath);
            manager.jobApply.hasResume = 1;
            
        }
        manager.ApplyJobApplication();
        Response.Redirect("Approve.aspx");
    }
    

}
