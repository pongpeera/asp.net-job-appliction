﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Apply.aspx.cs" Inherits="Apply" %>

<asp:Content ID="bodyContent" runat="server" ContentPlaceHolderID="bodyContent">
    <div class="content-center2 w3-display-topmiddle w3-grayscale-min">
        <div class="w3-white w3-hover-shadow ">
            <div class="form-group">
                <header class="w3-container w3-light-green">
                    <h2 style="color: white;">APPLY</h2>
                    
                </header>
                <!--
                <div class="w3-col" style="width: 30%;">
                        <asp:Button ID="applyBtton" runat="server" Text="สมัครงาน" CssClass="w3-btn w3-dark-grey" />
                    </div>
                -->
                <br />

                <div class="w3-container w3-opacity">
                    <div class="w3-col" style="width: 100%">
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <h6>
                                    <asp:Label ID="lbThaiFirstName" CssClass="w3-label" runat="server">ชื่อภาษาไทย</asp:Label></h6>
                                <asp:TextBox runat="server" ID="thaiFirstName" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>

                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <h6>
                                    <asp:Label ID="lbThaiLastName" CssClass="w3-label" runat="server">นามสกุลภาษาไทย</asp:Label></h6>

                                <asp:TextBox runat="server" ID="thaiLastName" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>
                    </div>


                    <div class="w3-col" style="width: 100%">
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbEnglishFirstName" CssClass="w3-label" runat="server">ชื่อภาษาอังกฤษ</asp:Label>
                                <asp:TextBox runat="server" ID="englishFirstName" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>

                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbEnglishLastName" CssClass="w3-label" runat="server">นามสกุลภาษาอังกฤษ</asp:Label>
                                <asp:TextBox runat="server" ID="englishLastName" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>

                    </div>

                    <div class="w3-col" style="width: 100%">
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbGender" CssClass="w3-label" runat="server">เพศ</asp:Label>
                                <select id="ddApplyGender" class="w3-select w3-border" runat="server">
                                    <option value="0">ชาย</option>
                                    <option value="1">หญิง</option>
                                </select>
                            </div>
                        </div>

                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbBirthdate" CssClass="w3-label" runat="server">วัน / เดือน / ปีเกิด</asp:Label>
                                <asp:TextBox runat="server" ID="Birthdate" CssClass="w3-input w3-border" TextMode="Date"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="w3-col" style="width: 100%">
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbAddress" CssClass="w3-label" runat="server">ที่อยู่</asp:Label>
                                <asp:TextBox runat="server" ID="address" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>

                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbTelephoneNumber" CssClass="w3-label" runat="server">หมายเลขโทรศัพท์ที่ติดต่อได้</asp:Label>
                                <asp:TextBox runat="server" ID="telephoneNumber" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="w3-col" style="width: 100%">
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbApplyEducation" CssClass="w3-label" runat="server">วุฒิการศึกษาสูงสุด</asp:Label>
                                <asp:DropDownList ID="ddApplyEducation" class="w3-select w3-border" runat="server">
                                    <asp:ListItem Value="มัธยมศึกษาปีที่ 6" Text="มัธยมศึกษาปีที่ 6"></asp:ListItem>
                                    <asp:ListItem Value="ปวช." Text="ปวช."></asp:ListItem>
                                    <asp:ListItem Value="ปวส." Text="ปวส."></asp:ListItem>
                                    <asp:ListItem Value="ป.ตรี" Text="ป.ตรี"></asp:ListItem>
                                    <asp:ListItem Value="ป.โท" Text="ป.โท"></asp:ListItem>
                                    <asp:ListItem Value="ป.เอก" Text="ป.เอก"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbEmail" CssClass="w3-label" runat="server">อีเมล (สำหรับติดตามผลการสมัครงาน)</asp:Label>
                                <asp:TextBox runat="server" ID="email" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="w3-col" style="width: 100%">
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbPassword" CssClass="w3-label" runat="server">รหัสผ่าน (สำหรับติดตามผลการสมัครงาน)</asp:Label>
                                <asp:TextBox runat="server" ID="password" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbConfirmPassword" CssClass="w3-label" runat="server">ยืนยันรหัสผ่าน (สำหรับติดตามผลการสมัครงาน)</asp:Label>
                                <asp:TextBox runat="server" ID="confirmPassword" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>
                    </div>


                    <div class="w3-col" style="width: 100%">
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbStartDate" CssClass="w3-label" runat="server">วันที่สามารถเริ่มงานได้</asp:Label>
                                <asp:TextBox runat="server" ID="startDate" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbSalaryRequirement" CssClass="w3-label" runat="server">เงินเดือนที่ต้องการ</asp:Label>
                                <asp:TextBox runat="server" ID="salaryRequirement" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="w3-col" style="width: 100%">
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbPersonalId" CssClass="w3-label" runat="server">หมายเลขบัตรประชาชน</asp:Label>
                                <asp:TextBox runat="server" ID="personalId" CssClass="w3-input w3-border"></asp:TextBox>
                            </div>
                        </div>
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbPosition" CssClass="w3-label" runat="server">ตำแหน่งงานที่ต้องการ</asp:Label>
                                <asp:DropDownList runat="server" CssClass="w3-select w3-border" ID="ddApplayJobPosition"></asp:DropDownList>
                            </div>
                        </div>
                        
                    </div>

                    <div class="w3-col" style="width: 100%">
                        
                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbProfileImage" CssClass="w3-label" runat="server">รูปถ่ายหน้าตรงขนาด 2 นิ้ว</asp:Label>
                                <asp:FileUpload runat="server" ID="profileImage" CssClass="w3-btn-block w3-teal"></asp:FileUpload>
                                <img id="preview" src="#" style="display: none; height:100%; width:100%" />
                            </div>
                        </div>

                        <div class="w3-col" style="width: 50%">
                            <div class="marginNormal">
                                <asp:Label ID="lbResume" CssClass="w3-label" runat="server">เรซูเม่</asp:Label>
                                <asp:FileUpload runat="server" ID="resume" CssClass="w3-btn-block w3-teal"></asp:FileUpload>
                            </div>
                        </div>
                    </div>

                    <div class="w3-col" style="width: 100%">
                        <div class="marginNormal">
                            <asp:Button ID="applyJobButton" runat="server" Text="ยืนยันการสมัคร" CssClass="w3-btn-block w3-dark-grey" OnClick="applyJobButton_Click" />
                        </div>
                        <br />

                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                function readURL(input) {

                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        console.log(input);
                        reader.onload = function (e) {
                            var img = $('#preview');
                            img.css("display", "initial");
                            img.attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#<%=profileImage.ClientID %>").on("change", function () {

                    readURL(this);
                });
            });

        </script>
    </div>
</asp:Content>
