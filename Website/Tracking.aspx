﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Tracking.aspx.cs" Inherits="Tracking" %>
<asp:Content ID="bodyContent" runat="server" ContentPlaceHolderID="bodyContent">
    <table class="table table-bordered">
        <tr>
            <td>ตำแหน่งงาน</td>
            <td>สถานะการรับการสมัคร</td>
            <td>สถานะการสัมภาษณ์งาน</td>
            <td>ผลการสมัครงาน</td>
            <td>ระยะเวลาที่ยืน</td>
            <td>วันที่เริ่มสมัคร</td>
        </tr>
        <% foreach (var apply in jobApply) { %>
            <tr>
                <td><%=apply.position.jobPositionName %></td>
                <td>
                    <%= (apply.isConfirm > 0) ? "ตรวจรับแล้ว":"รอการตรวจรับ" %>
                </td>
                <td>
                     <%= (apply.interviewList.Count > 0) ? apply.interviewList.FirstOrDefault().interviewDate + "-" +  apply.interviewList.FirstOrDefault().interviewTime :"รอ" %>
                </td>
                <td>
                    <%= (apply.result > 0)?  ((apply.result ==1) ? "ผ่าน" : "ไม่ผ่าน" ):"รอ" %>
                </td>
                <td><%=apply.applyDate %></td>
                <td><%=apply.applyDate %></td>
            </tr>
        <% } %>
    </table>
    <script type="text/javascript">
        $(document).ready(function () {
            $.map($("tr").not(":first"), function (tr) {
                moment.locale("th")
                var td = $(tr).find("td:eq(4)");
                var time = td.text();
                var countTime = moment(time, "DD/MM/YYYY").fromNow();
                td.text("สมัครมาแล้ว " + countTime);
            });
        });
    </script>
</asp:Content>
