﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Position.aspx.cs" Inherits="Position" %>
<asp:Content ID="bodyContent" runat="server" ContentPlaceHolderID="bodyContent">
    <div class="row">
        <div class="form-group">
            <asp:TextBox CssClass="form-control" ID="positionName" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:TextBox CssClass="form-control" ID="positionDescription" runat="server" TextMode="MultiLine" ></asp:TextBox>
        </div>

        <asp:Button CssClass="btn btn-primary" ID="btnPosition" runat="server" OnClick="btnPosition_Click"  Text="บันทึก" />
    </div>
    <asp:Label ID="lbAddJobPositionMsg" runat="server"></asp:Label>
    <ul>
        <% foreach (var job in jobs) { %>
            <li>
                <input type="hidden" value="<%= job.jobPositionId %>" />
                <span><%= job.jobPositionName %></span>
                <button>ลบ</button>

            </li>
        <% } %>
    </ul>
    <script type="text/javascript">
        $(document).ready(function () {

            setTimeout(function () {
                $("#<%=lbAddJobPositionMsg.ClientID%>").text("");
            }, 3000);
            $("li>button").on("click", function (e) {
                e.preventDefault();
                if (confirm("ต้องการลบหมือไร่")) {
                    var li = $(this).closest("li");
                    var id = li.find("[type=hidden]").val();
                    console.log(id);
                    $.ajax({
                        url: "AdminServices.asmx/DeleteJobPosition",
                        method: "POST",
                        data: { id: id },
                        success: function (response) {
                            if (response == "true") {
                                li.remove();
                            }
                        }
                    });
                }
                
            });
        });
    </script>
</asp:Content>