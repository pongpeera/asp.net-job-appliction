﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JobApplication.Manager;

public partial class CreateAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void createAdminButton_Click(object sender, EventArgs e)
    {
        JobLoginManager manager = new JobLoginManager();
        manager.login = new JobApplication.Bean.Login(adminEmail.Text,adminPassword.Text);
        bool result = manager.AddNewAdmin();
        string msg = (result) ? "เพิ่มผู้ดูแลระบบเรียบร้อย" : "ไม่สามารถเพิ่มผู้ดูแลระบบได้";
        adminEmail.Text = "";
        lbCreateAdminMsg.Text = msg;

    }
}