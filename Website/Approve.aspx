﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Approve.aspx.cs" Inherits="Approve" %>
<asp:Content ID="ApproveContent" runat="server" ContentPlaceHolderID="bodyContent">
    <div class="content-center2 w3-display-topmiddle w3-grayscale-min">
        <div class="w3-white w3-hover-shadow ">
    
            <table class="w3-table">
                <tr class="w3-green">
                    <td>วันที่สมัคร</td>
                    <td>ชื่อผู้สมัคร</td>
                    <td>อีเมลผู้สมัคร</td>
                    <td>การศึกษาสูงสุด</td>
                    <td>หมายเลขโทรศัพท์</td>
                    <td>เพศ</td>
                    <td>อายุ</td>
                    <td>ตำแหน่งาน</td>
                </tr>
                <% foreach (var apply in jobApplyList) { %>
                    <% if (apply.result == 0) { %>
                          <tr>
                            <td><%= apply.applyDate  %></td>
                            <td><%= apply.profile.thaiName+" "+apply.profile.thaiLastName  %></td>
                            <td><%= apply.profile.login.email%></td>
                            <td><%= apply.profile.education%></td>
                            <td><%= apply.profile.telephoneNumber%></td>
                            <td><%= (apply.profile.gender == 0)?"ชาย":"หญิง" %></td>
                            <td><%= apply.profile.birthdate %></td>
                            <td><%= apply.position.jobPositionName %></td>
                            
              
                    </tr>      
                    <% } %>
                    <tr>
                        <td colspan="8">
                            
                                
                                    <% if (apply.isConfirm == 0) { %>
                                        <asp:Button ID="confirmApply" runat="server" CssClass="w3-btn w3-red" ForeColor="White" Text="ตรวจรับใบสมัคร" />
                                        <!--<a href="#" id="confirmApply">ตรวจรับใบสมัคร</a>-->
                                    <% } %>
                                    <% if (apply.interviewList.Count == 0) { %>
                                        <asp:Button ID="confirmInterView" runat="server" CssClass="w3-btn w3-teal" ForeColor="White" Text="กำหนดสอบสัมภาษณ์" />
                                      <!--<a href="#" id="confirmInterView">กำหนดสอบสัมภาษณ์</a>-->
                                    <% }else { %>
                                        <asp:Button ID="interviewResult" runat="server" CssClass="w3-btn w3-brown" ForeColor="White" Text="กำหนดผลการสมัคร" />
                                       <!--<a href="#" id="interviewResult">กำหนดผลการสมัคร</a>-->
                                    <%} %>
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8"><hr /></td>
                    </tr>
                <%--<% } %>--%>
            </table>
    
        </div>
    </div>

    <div id="interviewModal" class="w3-modal">
        <div class="w3-modal-content w3-card-8 w3-animate-top">
            <header class="w3-container w3-teal">
                <span class="w3-closebtn">&times;</span>
                <h2></h2>
            </header>
            <div class="w3-container">
                <div class="form-group">
                        <asp:TextBox runat="server" ID="txtInterviewDate" CssClass="w3-input w3-border" TextMode="Date"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:TextBox runat="server" ID="txtInterviewTime" CssClass="w3-input w3-border"></asp:TextBox>
                    </div>
                    <asp:HiddenField ID="hiddenInterviewEmail" runat="server" Value="" />
                    <asp:HiddenField ID="hiddenInterviewApply" runat="server" Value="" />
            </div>
            <footer class="w3-container w3-teal">
                <asp:Button type="button" CssClass="w3-btn" runat="server" ID="interviewButton" OnClick="interviewButton_Click" Text="ยืนยันการนัดสัมภาษณ์"/>
            </footer>
        </div>
    </div>

    <div id="resultModal" class="w3-modal">
        <div class="w3-modal-content w3-card-8 w3-animate-top">
            <header class="w3-container w3-teal">
                <span class="w3-closebtn">&times;</span>
                <h2></h2>
            </header>
            <div class="w3-container">
                <asp:RadioButton id="confimResultRadio1" runat="server" text="ผ่าน" GroupName="confimResultRadioGroup" CssClass="w3-radio"></asp:RadioButton>
                <asp:RadioButton id="confimResultRadio2" runat="server" text="ไม่ผ่าน" GroupName="confimResultRadioGroup" CssClass="w3-radio"></asp:RadioButton>
                <asp:HiddenField ID="hiddenResultEmail" runat="server" Value="" />
            </div>
            <footer class="w3-container w3-teal">
                <asp:Button type="button" CssClass="w3-btn" runat="server" ID="resultButton" OnClick="resultButton_Click" Text="ยืนยันการผลการสัมภาษณ์"/>
            </footer>
        </div>
    </div>

   
    <script type="text/javascript">
        $(document).ready(function () {
            $(".w3-modal span").on("click", function (e) {
                e.preventDefault();
                $(this).closest(".w3-modal").css("display", "none");
            });

            $.map($("tr").not(':first'), function (tr) {
                var td = $(tr).find("td:eq(6)");
                var age = moment().diff(td.text(),"years");
                td.text(age);
            });

            $("a#confirmInterView").on("click", function (e) {
                e.preventDefault();
                var self = $(this);
                var tr = self.closest("tr");
                var apply = tr.find("td:eq(0)").text();
                var name = tr.find("td:eq(1)").text();
                var email = tr.find("td:eq(2)").text();
                var modal = $("#interviewModal");
                modal.find("#bodyContent_hiddenInterviewEmail").val(email);
                modal.find("#bodyContent_hiddenInterviewApply").val(apply);
                modal.find("h2").text("กำหนดผลการสมัครงาน คุณ " + name);
                modal.css("display","block");
            });

            $("a#interviewResult").on("click", function (e) {
                e.preventDefault();
                var self = $(this);
                var tr = self.closest("tr");
                var email = tr.find("td:eq(2)").text();
                var modal = $("#resultModal");
                modal.find("#bodyContent_hiddenResultEmail").val(email);
                modal.find("h2").text("ยืนยันผลการสมัครงาน คุณ " + name);
                modal.css("display", "block");
            });
            
            $("a#confirmApply").on("click", function (e) {
                e.preventDefault();
                var self = $(this);
                var tr = self.closest("tr");
                var email = tr.find("td:eq(2)").text();
                $.ajax({
                    url: "AdminServices.asmx/ConfirmApply",
                    method: "POST",
                    data: { email: email },
                    success: function (response) {
                        if (response == "true") {
                            self.remove();
                        }
                    }
                });
            });

        });
    </script>
</asp:Content>
