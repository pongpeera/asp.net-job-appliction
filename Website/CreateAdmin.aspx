﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CreateAdmin.aspx.cs" Inherits="CreateAdmin" %>
<asp:Content ID="CreateAdminContent" runat="server" ContentPlaceHolderID="bodyContent">
     <div class="form-group">
            <asp:TextBox CssClass="form-control" ID="adminEmail" runat="server"></asp:TextBox>
     </div>
     <div class="form-group">
            <asp:TextBox CssClass="form-control" ID="adminPassword" runat="server" TextMode="Password"></asp:TextBox>
     </div>
    <div class="form-group">
           <asp:TextBox CssClass="form-control" ID="ConfirmAdminPassword" runat="server" TextMode="Password"></asp:TextBox>
     </div>
     <asp:Button Text="บันทึกข้อมูล" ID="createAdminButton" OnClick="createAdminButton_Click" runat="server"/>
    <asp:Label ID="lbCreateAdminMsg" runat="server"></asp:Label>

    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $("#<%=lbCreateAdminMsg.ClientID%>").text("");
            }, 3000);
            
        });
    </script>
</asp:Content>
