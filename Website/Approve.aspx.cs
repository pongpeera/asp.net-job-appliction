﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JobApplication.Manager;
using JobApplication.Bean;

public partial class Approve : System.Web.UI.Page
{
    public List<JobApply> jobApplyList;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {

        }
        LoadApply();

    }

    private void LoadApply()
    {
        JobApplyManager manager = new JobApplyManager();
        jobApplyList = manager.GetAllApply();
    }

    protected void interviewButton_Click(object sender, EventArgs e)
    {
        JobApplyManager manager = new JobApplyManager();
        manager.jobApply.interviewList.Add(new Interview(txtInterviewDate.Text, txtInterviewTime.Text));
        manager.jobApply.profile.login.email = hiddenInterviewEmail.Value;
        manager.jobApply.applyDate = hiddenInterviewApply.Value;

        manager.ConfirmInterview();
        //manager.SendMail("นัดสอบสัมภาษณ์","รายละเอียดสามารถเข้าสู่ระบบตรวจสอบภายในเว็บไซด์บริษัท");
        LoadApply();
    }

    
    protected void resultButton_Click(object sender, EventArgs e)
    {
        int result = (confimResultRadio1.Checked) ? 1 : 2;
        JobApplyManager manager = new JobApplyManager();
        manager.jobApply.result = result;
        manager.jobApply.profile.login.email = hiddenResultEmail.Value;

        manager.ConfirmResult();
        //manager.SendMail("ผลการสมัครงาน","สามารถตรวจสอบได้จากเว็บไซด์บริษัท");
        LoadApply();
    }
}