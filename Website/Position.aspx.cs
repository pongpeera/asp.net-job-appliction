﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JobApplication.Manager;
using JobApplication.Bean;

public partial class Position : System.Web.UI.Page
{
    public List<JobPosition> jobs;
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadJobPosition();
    }

    protected void btnPosition_Click(object sender, EventArgs e)
    {
        JobPosition job = new JobPosition(positionName.Text,positionDescription.Text);
        JobPositionManager manager = new JobPositionManager();
        manager.jobPosition = job;
        bool result = manager.AddJobPosition();
        string msg = (result) ? "เพิ่มตำแหน่งงานเรียบร้อยแล้ว" : "เกิดข้อผิดพลาดไม่สามารถเพิ่มตำปหน่งงานได้";
        positionName.Text = "";
        positionDescription.Text = "";
        lbAddJobPositionMsg.Text = msg;
        LoadJobPosition();
    }

    private void LoadJobPosition()
    {
        JobPositionManager manager = new JobPositionManager();
        jobs = manager.GetAllJobPosition();
    }
}