﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JobApplication.Manager;


public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {
           
        }
    }

    protected void loginButton_Click(object sender, EventArgs e)
    {
        
        JobLoginManager manager = new JobLoginManager();
        manager.login.email = loginEmail.Text;
        manager.login.password = loginPassword.Text;
        if (manager.verifyLogin())
        {
            Session["email"] = loginEmail.Text;
            string destination = (manager.login.status > 0) ? "~/Tracking.aspx" : "~/Approve.aspx";
            Response.Redirect(destination);
        }
        else
        {
            lbLoginEmail.Text = "ไม่สามารถเข้าสู่ระบบได้";
        }
    }
}