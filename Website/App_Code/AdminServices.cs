﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using JobApplication.Manager;
using JobApplication.Bean;

/// <summary>
/// Summary description for AdminServices
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class AdminServices : System.Web.Services.WebService
{

    public AdminServices()
    {

    }

    [WebMethod]
    public void DeleteJobPosition(int id)
    {
        JobPositionManager manager = new JobPositionManager();
        manager.jobPosition = new JobPosition(id);
        string result = (manager.RemoveJobPosition())?"true":"false";
        this.Context.Response.Write(result);
    }

    [WebMethod]
    public void ConfirmApply(string email)
    {
        JobApplyManager manager = new JobApplyManager();
        manager.jobApply.profile.login.email = email;
        string result = (manager.ConfirmApply()) ? "true" : "false";
        this.Context.Response.Write(result);
    }

}
