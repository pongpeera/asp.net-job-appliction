﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JobApplication.Manager;
using JobApplication.Bean;

public partial class Tracking : System.Web.UI.Page
{
    public List<JobApply> jobApply;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {
            
        }
        LoadTracking();
    }
    private void LoadTracking()
    {
        JobApplyManager manager = new JobApplyManager();
        if (Session["email"] == null) Response.Redirect("~/Login.aspx");
        manager.jobApply.profile.login.email = Session["email"].ToString();
        jobApply = manager.GetApply();
    }
}