﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<asp:Content ID="bodyContent" runat="server" ContentPlaceHolderID="bodyContent">

    <div class="row">


        <div class="col-md-6">
            <div class="middle-right">
                <a href="Login.aspx">
                    <img src="Images/login_button.png" />
                </a>
            </div>
        </div>

        <div class="col-md-6">
            <div class="middle-left">
                <a href="Apply.aspx">
                    <img src="Images/apply_button.png" />
                </a>
            </div>
        </div>
    </div>
</asp:Content>
